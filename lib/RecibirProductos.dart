import 'Producto.dart';

import "Tarea.dart";

class RecibirProductos extends Tarea {
  final int maxProductos;
  final String provedor;
  final int id;
  final int entregado;

  RecibirProductos(Producto producto, DateTime fecha, this.maxProductos,
      this.provedor, this.id, this.entregado)
      : super(producto, fecha);

  @override
  String header() {
    return "Recibir Productos";
  }

  @override
  String body() {
    return "Producto: " + super.producto.nombreProducto;
  }

  @override
  String bottom() {
    return "Proveedor: " + this.provedor;
  }

  @override
  String corner() {
    return "0/" + this.maxProductos.toString();
  }
}
