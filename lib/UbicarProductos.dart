import 'Producto.dart';
import "Tarea.dart";

class UbicarProductos extends Tarea {
  final int numeroDeProductos;
  final int estado;

  UbicarProductos(
      Producto producto, DateTime fecha, this.numeroDeProductos, this.estado)
      : super(producto, fecha);

  @override
  String header() {
    return "Ubicar Productos";
  }

  @override
  String body() {
    return "Numero De Items: " + this.numeroDeProductos.toString();
  }

  @override
  String bottom() {
    String nombreDeProducto = this.producto.nombreProducto.toString();
    return "Producto: " + nombreDeProducto;
  }

  @override
  String corner() {
    if (this.estado == 0) {
      return "Pendiente";
    }
    return "Ubicado";
  }
}
