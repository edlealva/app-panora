class Producto {
  final String codigoDeBarras;
  final String nombreProducto;
  final String estadoActual;
  final String categoria;

  Producto(this.codigoDeBarras, this.nombreProducto, this.estadoActual,
      this.categoria);
}
