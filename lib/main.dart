import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:panoraWMS/LoginPage.dart';
import 'package:panoraWMS/RecibirProductos.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'EntregarProductos.dart';
import 'RecibirProductos.dart';
import 'LoginPage.dart';
import 'Producto.dart';
import 'Tarea.dart';
import 'UbicarProductos.dart';
import 'constants/constants.dart' as Constants;

/*prueba dev */

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Panora WMS",
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      theme: ThemeData(accentColor: Colors.white70),
    );
  }
}

class MainPage extends StatefulWidget {
  final Data data1;
  MainPage(this.data1);
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  SharedPreferences sharedPreferences;

  Future<List<Tarea>> _getTareas() async {
    List<Tarea> myTasks = [];
    final response = await http.get(
      Uri.encodeFull(Constants.entrada),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Token " + widget.data1.token
      },
    );

    if (response.statusCode == 200) {
      final List<dynamic> jsonResponse = json.decode(response.body);
      print(jsonResponse);
      for (var datita in jsonResponse) {
        if (datita["UserPerchero"] == widget.data1.id) {
          final responseP = await http.get(
            Uri.encodeFull(Constants.producto),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Token " + widget.data1.token
            },
          );
          if (responseP.statusCode == 200) {
            final List<dynamic> jsonResponseP = json.decode(responseP.body);
            for (var productoJ in jsonResponseP) {
              if (productoJ["entrega"] == datita["id"]) {
                final responseC = await http.get(
                  Uri.encodeFull(Constants.categoria),
                  headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Token " + widget.data1.token
                  },
                );
                if (responseC.statusCode == 200) {
                  final List<dynamic> jsonResponseC =
                      json.decode(responseC.body);
                  for (var categoriaJ in jsonResponseC) {
                    if (categoriaJ['id'] == productoJ["categoria"]) {
                      final responseProv = await http.get(
                        Uri.encodeFull(Constants.proveedor),
                        headers: {
                          "Content-Type": "application/json",
                          "Authorization": "Token " + widget.data1.token
                        },
                      );
                      if (responseProv.statusCode == 200) {
                        final List<dynamic> jsonResponseProv =
                            json.decode(responseProv.body);
                        for (var proveedorJ in jsonResponseProv) {
                          if (proveedorJ['id'] == productoJ['Proovedor']) {
                            DateTime fecha =
                                DateTime.parse(datita['Fecha'].toString());
                            Producto producto = new Producto(
                                productoJ["Codigo_barra"],
                                productoJ["Nombre"],
                                productoJ["Estado"],
                                categoriaJ["Nombre"]);
                            RecibirProductos r = new RecibirProductos(
                                producto,
                                fecha,
                                categoriaJ['UnidadesExistencia'],
                                proveedorJ['Nombre'],
                                datita['id'],
                                0);
                            myTasks.add(r);
                          }
                        }
                      }
                    }
                  }
                }

                break;
              }
            }
          }
        }
      }

      final responseSalida = await http.get(
        Uri.encodeFull(Constants.salida),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Token " + widget.data1.token
        },
      );

      if (responseSalida.statusCode == 200) {
        List<dynamic> jsonResponseSalida = json.decode(responseSalida.body);
        for (var salidaJ in jsonResponseSalida) {
          if (salidaJ['UserPerchero'] == widget.data1.id) {
            final responseProducto = await http.get(
              Uri.encodeFull(Constants.producto),
              headers: {
                "Content-Type": "application/json",
                "Authorization": "Token " + widget.data1.token
              },
            );
            if (responseProducto.statusCode == 200) {
              List<dynamic> jsonResponseProducto =
                  json.decode(responseProducto.body);
              for (var productoJ in jsonResponseProducto) {
                if (productoJ['salida'] == salidaJ['id']) {
                  final responseCategoria = await http.get(
                    Uri.encodeFull(Constants.categoria),
                    headers: {
                      "Content-Type": "application/json",
                      "Authorization": "Token " + widget.data1.token
                    },
                  );
                  if (responseCategoria.statusCode == 200) {
                    List<dynamic> jsonResponseCat =
                        json.decode(responseCategoria.body);
                    for (var categoriaJ in jsonResponseCat) {
                      if (categoriaJ['id'] == productoJ['categoria']) {
                        final responseFactura = await http.get(
                          Uri.encodeFull(Constants.factura),
                          headers: {
                            "Content-Type": "application/json",
                            "Authorization": "Token " + widget.data1.token
                          },
                        );
                        if (responseFactura.statusCode == 200) {
                          List<dynamic> jsonResponseCliente =
                              json.decode(responseFactura.body);
                          for (var facturaJ in jsonResponseCliente) {
                            if (facturaJ['id'] == productoJ['factura_venta']) {
                              final responsePerchado = await http.get(
                                Uri.encodeFull(Constants.perchado),
                                headers: {
                                  "Content-Type": "application/json",
                                  "Authorization": "Token " + widget.data1.token
                                },
                              );
                              if (responsePerchado.statusCode == 200) {
                                List<dynamic> jsonResponsePerchado =
                                    json.decode(responsePerchado.body);
                                for (var perchadoJ in jsonResponsePerchado) {
                                  if (perchadoJ['id'] ==
                                      productoJ['perchado']) {
                                    final responseUbicacion = await http.get(
                                      Uri.encodeFull(Constants.ubicacion),
                                      headers: {
                                        "Content-Type": "application/json",
                                        "Authorization":
                                            "Token " + widget.data1.token
                                      },
                                    );
                                    if (responseUbicacion.statusCode == 200) {
                                      List<dynamic> jsonUbicacion =
                                          json.decode(responseUbicacion.body);
                                      for (var ubicacionJ in jsonUbicacion) {
                                        if (ubicacionJ['id'] ==
                                            perchadoJ['ubication']) {
                                          DateTime fecha =
                                              DateTime.parse(salidaJ['fecha']);
                                          Producto producto = new Producto(
                                              productoJ['Codigo_barra'],
                                              productoJ['Nombre'],
                                              productoJ['Estado'],
                                              categoriaJ['Nombre']);
                                          EntregarProductos entrega =
                                              new EntregarProductos(
                                                  producto,
                                                  fecha,
                                                  ubicacionJ['Nombre'],
                                                  0,
                                                  facturaJ['Comprador'],
                                                  facturaJ['Num_factura']);
                                          myTasks.add(entrega);
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }

                  break;
                }
              }
            }
          }
        }
      }

      return myTasks;
    } else {
      throw Exception('Failed to load jobs from API');
    }
  }

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mis Tareas", style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.indigo[900],
      ),
      body: Container(
        child: FutureBuilder(
            future: _getTareas(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                print(snapshot.data);
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.indigo[900]),
                    ),
                  ),
                );
              } else {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) =>
                        buildTasksCard(context, index, snapshot.data));
              }
            }),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.indigo[900],
              ),
              child: Text(
                'Bienvenido ${widget.data1.nombre} \n ${widget.data1.apellido}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.message),
              title: Text('Messages'),
            ),
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text('Profile'),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesión'),
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginPage()),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTasksCard(BuildContext context, int index, dynamic data) {
    return Container(
      child: Card(
        color: Colors.grey.shade200,
        child: InkWell(
          splashColor: Colors.grey.withAlpha(255),
          onTap: () {
            print('Card tapped.');
          },
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.indigo.shade900,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.fiber_manual_record,
                          color: Colors.green.shade700),
                      Text(
                        data[index].header(),
                        style: TextStyle(color: Colors.white),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 8.0, top: 4.0, bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Text(data[index].body()),
                      Spacer(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Text(data[index].bottom()),
                      Spacer(),
                      Text(data[index].corner()),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
