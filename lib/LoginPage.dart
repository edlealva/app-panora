import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
//import 'package:dio/dio.dart';
import 'package:panoraWMS/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'constants/constants.dart' as Constants;

//import 'main.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class Data {
  String nombre = '';
  String apellido = '';
  String token;
  int id;

  Data({this.nombre, this.apellido, this.token, this.id});
}

class userValidator {
  static String validate(String value) {
    return value.isEmpty ? 'User can\'t be empty' : null;
  }
}

class passwordValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Password can\'t be empty' : null;
  }
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  String nombre;
  String apellido;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent));
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.indigo[900], Colors.indigo[900]],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter),
        ),
        child: _isLoading
            ? Center(child: CircularProgressIndicator())
            : ListView(
                children: <Widget>[
                  headerSection(),
                  textSection(),
                  buttonSection(),
                  BottomSection(),
                ],
              ),
      ),
    );
  }

  Future signIn(String username, password) async {
    String nombre = '';
    String apellido = '';
    String token;
    int id;
    Data data1;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = jsonEncode({"username": username, "password": password});
    final response = await http.post(
      Uri.encodeFull(Constants.auth),
      headers: {"Content-Type": "application/json"},
      body: data,
    );
    if (response.statusCode == 200) {
      final Map parsed = json.decode(response.body);
      nombre = parsed['Nombre'].toString();
      apellido = parsed['Apellido'].toString();
      token = parsed['token'].toString();
      id = parsed['user_id'];
      data1 = Data(nombre: nombre, apellido: apellido, token: token, id: id);
      if (parsed != null) {
        setState(() {
          _isLoading = false;
        });
        sharedPreferences.setString("token", 'token');
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => MainPage(
                      data1,
                    )),
            (Route<dynamic> route) => false);
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      _showAlertDialog();
    }
  }

  void _showAlertDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('Usuario o contraseña incorrecto'),
            content: new Text(
                'Por favor introduzca su usuario y contraseña correctamente'),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: new Text('Aceptar'))
            ],
          );
        });
  }

  Container buttonSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 40.0,
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      margin: EdgeInsets.only(top: 15.0),
      child: RaisedButton(
        onPressed: emailController.text == "" || passwordController.text == ""
            ? null
            : () {
                setState(() {
                  _isLoading = true;
                });
                signIn(emailController.text, passwordController.text);
              },
        elevation: 0.0,
        color: Colors.indigoAccent[100],
        child: Text("Iniciar sesión", style: TextStyle(color: Colors.white)),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: emailController,
            validator: userValidator.validate,
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white70),
            decoration: InputDecoration(
              icon: Icon(Icons.account_circle, color: Colors.white),
              hintText: "Usuario",
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white70)),
              hintStyle: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            controller: passwordController,
            validator: passwordValidator.validate,
            cursorColor: Colors.white,
            obscureText: true,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              icon: Icon(Icons.lock, color: Colors.white),
              hintText: "Contraseña",
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white70)),
              hintStyle: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Container headerSection() {
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
      child: Image.asset('assets/images/logo.jpeg'),
    );
  }

  Container BottomSection() {
    return Container(
        padding: EdgeInsets.only(top: 24),
        child: new InkWell(
            child: new Text(
              'Olvidaste tu contraseña?',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.cyanAccent),
            ),
            onTap: () =>
                launch('https://app.panoraec.com/recuperar-password')));
  }
}
