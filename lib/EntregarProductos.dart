import 'Producto.dart';

import "Tarea.dart";

class EntregarProductos extends Tarea {
  final String posicion;
  final int estado;
  final String nombreCliente;
  String numeroDeFactura;
  EntregarProductos(Producto producto, DateTime fecha, this.posicion,
      this.estado, this.nombreCliente, this.numeroDeFactura)
      : super(producto, fecha);
//Comentario para probar integración continua
//Segundo commit de prueba
  @override
  String header() {
    return "Orden De Despacho";
  }

  @override
  String body() {
    String nombreProducto = this.producto.nombreProducto;
    return "Ubicacion: " + this.posicion + "\nProducto: " + nombreProducto;
  }

  @override
  String bottom() {
    return "Cliente: " + this.nombreCliente;
  }

  @override
  String corner() {
    if (this.estado == 0) {
      return "No Entregado";
    }
    return "Entregado";
  }
}
