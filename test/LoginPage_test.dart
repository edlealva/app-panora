import 'package:flutter_test/flutter_test.dart';
import 'package:panoraWMS/LoginPage.dart';

void main() {
  test('Empty user returns error string', () {
    var result = userValidator.validate('');
    expect(result, 'User can\'t be empty');
  });
  test('Non-empty user returns null', () {
    var result = userValidator.validate('user');
    expect(result, null);
  });
  test('Empty password returns error string', () {
    var result = passwordValidator.validate('');
    expect(result, 'Password can\'t be empty');
  });
  test('Non-empty password returns null', () {
    var result = passwordValidator.validate('password');
    expect(result, null);
  });
}
